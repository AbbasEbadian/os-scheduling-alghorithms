#include<iostream>
#include<conio.h>
using namespace std;

struct Process{
	int process_id;
	int arrival_time;
	int execution_time;
	int execution_time_remaining;
	int turn_time;
	int complete_time;
	int wait_time;
	Process(){
		
	}
	Process(int id, int at, int et){
		process_id = id;
		arrival_time = at;
		execution_time = et;
		execution_time_remaining= et;
		turn_time=0;
		complete_time=0;
		wait_time=0;
		
	}
};
int current_time=0;
int process_count;
Process process_list[100];
float average_turn_time, average_wait_time;
int main(){
	
	cout<<"\nProcess count: ";
	cin>>process_count;

	cout<< endl <<"Enter " <<process_count  << " processes\n";
	for(int x=0; x<process_count; x++){
		cout<<"\nProcess "<<x+1;
		
	    int at, et;
	    cout<<"\nArrival_time=";
	    cin >> at;
	
		cout << "Execution time=";
	    cin >> et;
	    
	    Process p = Process(x+1, at, et);
	    process_list[x] = p;
	}
	for(int i=0; i<process_count; i++){
		for (int j=0; j<process_count-1;j++){
			bool res =  process_list[j+1].arrival_time< process_list[j].arrival_time;
			if (res){
				swap(process_list[j+1], process_list[j]);
			}	
		}
	}
	cout<<endl<<"GANTT"<<endl<< "["<<process_list[0].arrival_time;
	current_time = process_list[0].arrival_time;
	Process * current_process;
	int highest_response_ratio = -1;
	for (int x=0; x<=process_count; x++){
		if (process_list[x].arrival_time <= current_time && process_list[x].execution_time_remaining >0 ){
			int ratio = process_list[x].wait_time / process_list[x].execution_time;
			if (ratio > highest_response_ratio){
				highest_response_ratio = ratio;
				current_process = &process_list[x];
			}
		}
	}
	do{
		
	    current_time +=  current_process->execution_time_remaining;
	   	for (int x=0; x<=process_count; x++){
			if (process_list[x].arrival_time <= current_time &&
				process_list[x].execution_time_remaining >0 &&
				process_list[x].process_id != current_process->process_id){
				process_list[x].wait_time += current_time - current_process->arrival_time;
			}
		}
        current_process->execution_time_remaining=0;
        current_process->complete_time = current_time;
        current_process->turn_time = current_process->complete_time - current_process->arrival_time;
   		current_process->wait_time = current_process->turn_time-current_process->execution_time;
	   	
	        
	   
	    cout<<"]-P"<<current_process->process_id<<"-["<< current_time;
		current_process = NULL;
		highest_response_ratio = -1;
		for (int x=0; x<=process_count; x++){
			if (process_list[x].arrival_time <= current_time && process_list[x].execution_time_remaining >0 ){
				int ratio = process_list[x].wait_time / process_list[x].execution_time;
				if (ratio > highest_response_ratio){
					highest_response_ratio = ratio;
					current_process = &process_list[x];
				}
			}
		}
	}while(current_process != NULL);
	cout << "]" << endl;
	cout<<"\n\nPROCESS\t AT\t BT\t CT\t TT\t WT\n";
	for(int x=0; x < process_count ; x++){
	    current_process = &process_list[x];
	    cout<<"P"<<current_process->process_id <<" \t "<<current_process->arrival_time<<" \t "<<current_process->execution_time;
	    cout <<" \t "<<current_process->complete_time<<" \t "<<current_process->turn_time<<" \t "<< current_process->wait_time<<"\n";
	    average_wait_time += current_process->wait_time;
	    average_turn_time += current_process->turn_time;
	}
	cout<<"\nAVERAGE Turn Around Time: "<<average_turn_time/process_count<<"\nAVERAGE Wait Time: "<<average_wait_time/process_count;

	return 0;
}
